port module Main exposing (..)

import Browser
import Markdown
import Html exposing (Html, text, div, h1, label, textarea, header, h2, select, option, input, footer, ul, li)
import Html.Attributes exposing (src, class, value, type_, id, style)
import Html.Events exposing (onInput)


---- MODEL ----

type Tab
    = Edit
    | Preview


type alias Model =
    { text : String
    , activeTab : Tab
    , history : List String
    }

type alias Flags =
    String

init : Flags -> ( Model, Cmd Msg )
init flags =
    ( Model flags Edit [ flags ], Cmd.none )



---- UPDATE ----
port updateText : (String -> msg) -> Sub msg
port updateHistory : (String -> msg) -> Sub msg

type Msg
    = UpdateText String
    | UpdateHistory String
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateText text ->
            ( { model | text = text }, Cmd.none )

        UpdateHistory text ->
            ( { model | history = model.history ++ [text] }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model =
    div [ class "flex flex-col h-screen" ]
        [ header [ class "py-2 h-8 flex bg-gray-800" ]
            [ h2 [ class "m-0 text-white" ]
                [ text "マークダウンエディタ"]
            ]
        , div [ class "flex flex-col h-full" ]
            [ div [ class "flex flex-grow" ]
                [ div [ class "flex flex-grow flex-row flex-wrap" ]
                    [ div [ class "flex flex-grow flex-shrink sm:flex-basis-100 lg:flex-basis-25 relative" ]
                        [ textarea
                            [ id "editor"
                            , class "hidden"
                            ]
                            [ text model.text ]
                        ]
                    , div [ class "flex-grow flex-shrink flex-basis-25 relative" ]
                        [ div [ class "absolute inset-0 overflow-scroll py-0 px-4 border-solid border-2 border-gray-200"]
                            (Markdown.toHtml Nothing model.text)
                        ]
                    ]
                ]
            , footer [ class "relative" ]
                [ div [] [text "Footer"] ]
            ]
        ]



---- PROGRAM ----

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch [ updateText UpdateText, updateHistory UpdateHistory ]



main : Program Flags Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
