import "./main.css";
import { Elm } from "./Main.elm";
import registerServiceWorker from "./registerServiceWorker";
import CodeMirror from "codemirror";
import { ImmortalDB } from "immortal-db"
import "codemirror/lib/codemirror.css";

ImmortalDB.get('text', "").then(value => {
  const app = Elm.Main.init({
    node: document.getElementById("root"),
    flags: value,
  });

  const initializer = setInterval(() => {
    const textarea = document.getElementById("editor");
    if (!textarea) {
      return;
    }
    const editor = CodeMirror.fromTextArea(textarea, {
      lineNumbers: true,
      mode: "null"
    });

    editor.on("change", editor => {
      const text = editor.doc.getValue();
      app.ports.updateText.send(text);

      ImmortalDB.set("text", text);
    });

    editor.on("blur", editor => {
      const text = editor.doc.getValue();

      app.ports.updateHistory.send(text);
    });

    clearInterval(initializer);
  }, 10);

  registerServiceWorker();
});
