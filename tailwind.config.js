module.exports = {
  corePlugins: {
    preflight: false,
  },
  theme: {
    extend: {},
    screens: {
      'sm': {'max': '749px'},
      'lg': {'min': '750px'},
    },
  },
  variants: {},
  plugins: [],
}
